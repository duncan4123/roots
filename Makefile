export ROOTS_PATH := $(CURDIR)
export PROJECT_PATH=$(ROOTS_PATH)

# Change path if project is imported by another project
ifneq ($(notdir $(CURDIR)),roots)
ROOTS_PATH_RAW = $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
ROOTS_PATH := $(patsubst %/,%,$(ROOTS_PATH_RAW))
PROJECT_PATH=$(patsubst .roots,%,$(CURDIR))
endif

USER = $(shell whoami)
ifeq ($(USER),root)
SUDO=
else
SUDO=sudo
endif

export SELF = $(MAKE)
export PATH := $(ROOTS_PATH)/vendor:$(PATH)

export OS = $(shell uname -s | tr '[:upper:]' '[:lower:]')
export OS_ID = $(shell lsb_release --id | awk '{print $$3}')
export OS_RELEASE = $(shell lsb_release --release | awk '{print $$2}')

export PROJECT_DOCS_PATH ?= $(PROJECT_PATH)/docs

export ROOTS_BIN_PATH = $(ROOTS_PATH)/bin
export ROOTS_LIBRARY_PATH = $(ROOTS_PATH)/library
export ROOTS_VENDOR_PATH = $(ROOTS_PATH)/vendor
export ROOTS_VENV_PATH = $(ROOTS_PATH)/.venv

-include $(ROOTS_PATH)/Makefile.helpers
-include $(ROOTS_PATH)/library/*/Makefile*

.PHONY: wait-system
wait-system:
	$(shell while $(SUDO) fuser /var/{lib/{dpkg,apt/lists},cache/apt/archives}/lock >/dev/null 2>&1; do sleep $(shell shuf -i 1-5 -n 1); done)

.PHONY: init
init: wait-system
ifeq ($(OS_ID),Ubuntu)
	@$(SUDO) apt-get update && $(MAKE) wait-system && \
	$(SUDO) apt-get -qq install --yes build-essential libssl-dev libffi-dev python3 python3-dev python3-pip python3-venv && \
	python3 -m venv $(ROOTS_VENV_PATH) && \
	( \
		source $(ROOTS_VENV_PATH)/bin/activate; \
		pip install --upgrade pip; \
		pip install ansible; \
		pip install awscli; \
		python --version; \
		ansible --version; \
		aws --version; \
	)
endif

.PHONY: version
## Displays versions of many vendors installed
version:
ifeq ($(OS_ID),Ubuntu)
	@( \
		source $(ROOTS_VENV_PATH)/bin/activate; \
		python --version; \
		ansible --version; \
		aws --version; \
	)
endif

.PHONY: info
info:
	$(info USER: $(USER))
	$(info OS: $(OS))
	$(info OS_ID: $(OS_ID))
	$(info OS_RELEASE: $(OS_RELEASE))
	$(info PROJECT_PATH: $(PROJECT_PATH))
	$(info ROOTS_PATH: $(ROOTS_PATH))
	$(info ROOTS_BIN_PATH: $(ROOTS_BIN_PATH))
	$(info ROOTS_LIBRARY_PATH: $(ROOTS_LIBRARY_PATH))
	$(info ROOTS_VENDOR_PATH: $(ROOTS_VENDOR_PATH))
	$(info ROOTS_VENV_PATH: $(ROOTS_VENV_PATH))
	@$(SELF) version

.PHONY: update
## Updates roots
update:
	@[ -d "$(ROOTS_PATH)" ] && cd "$(ROOTS_PATH)" && git pull

.PHONY: clean
## Clean roots
clean:
	@[ "$(ROOTS_PATH)" == '/' ] || \
	 [ "$(ROOTS_PATH)" == '.' ] || \
	 	rm -rf $(ROOTS_PATH)

#!/usr/bin/env bash

# Purpose: Allow testing commands locally
# Usage: ./bin/roots.sh terraform/install

cd ../builder/ && "${1}"
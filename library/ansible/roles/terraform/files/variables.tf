variable "namespace" {
  type        = "string"
  default     = "pi"
  description = "Namespace, which could be your organization name, e.g. 'pi' (from PlanIt) or 'cp' (from CloudPosse)"
}

variable "stage" {
  type        = "string"
  description = "Stage (environment abbreviation), e.g. 'prd', 'stg', 'dev' or 'tst'"
}

variable "environment" {
  type        = "string"
  description = "Stage, e.g. 'production', 'staging', 'development' or 'testing'"
}

variable "name" {
  type        = "string"
  description = "Solution name, e.g. 'app' or 'cluster'"
}

variable "delimiter" {
  type        = "string"
  default     = "-"
  description = "Delimiter to be used between `name`, `namespace`, `stage`, etc."
}

variable "attributes" {
  type        = "list"
  default     = []
  description = "Additional attributes (e.g. `1`)"
}

variable "tags" {
  type        = "map"
  default     = {}
  description = "Additional tags (e.g. `map('BusinessUnit`,`XYZ`)"
}

variable "region" {
  type        = "string"
  default     = "ap-southeast-2"
  description = "AWS Region"
}

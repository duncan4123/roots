output "namespace" {
  value = "${var.namespace}"
}

output "stage" {
  value = "${var.stage}"
}

output "environment" {
  value = "${var.environment}"
}

output "name" {
  value = "${var.name}"
}

output "delimiter" {
  value = "${var.delimiter}"
}

output "attributes" {
  value = "${var.attributes}"
}

output "tags" {
  value = "${var.tags}"
}

output "region" {
  value = "${var.region}"
}

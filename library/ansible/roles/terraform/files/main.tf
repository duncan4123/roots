provider "aws" {
  region = "${var.region}"
}

terraform {
  required_version = ">= 0.11.14"
  backend          "s3"             {}
}

module "label" {
  source     = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=master"
  namespace  = "${var.namespace}"
  name       = "${var.name}"
  stage      = "${var.stage}"
  delimiter  = "${var.delimiter}"
  attributes = "${var.attributes}"
  tags       = "${var.tags}"
}

{{- defineDatasource "contributing" .Env.CONTRIBUTING -}}
{{- defineDatasource "commercial" .Env.COMMERCIAL -}}
{{- defineDatasource "config" .Env.README_YAML | regexp.Replace ".*" "" -}}
{{- defineDatasource "includes" .Env.README_INCLUDES | regexp.Replace ".*" "" }}

![logo][logo]

{{ if has (ds "config") "pipeline_status" }}
{{ if eq (ds "config").pipeline_status "true" }}
[![pipeline status](https://gitlab.com/{{(ds "config").repo }}/badges/master/pipeline.svg)](https://gitlab.com/{{(ds "config").repo }}/commits/master)
{{ end }}
{{ end }}

{{ if has (ds "config") "name" }} 
# {{(ds "config").name }} 
{{ end }}

{{ if has (ds "config") "description" }} 
{{(ds "config").description }} 
{{ end }}

---

{{ if has (ds "config") "screenshots" }}
## Screenshots

{{ range $screenshot := (ds "config").screenshots }}
{{ printf "![%s](%s) |\n|:--:|\n| *%s* |\n\n" $screenshot.name $screenshot.url $screenshot.description }}{{ end }}

{{ end }}

{{ if has (ds "config") "screenshots_raw" }}
## Screenshots

{{(ds "config").screenshots_raw }} 

{{ end }}


{{ if has (ds "config") "requirements" }}
## Requirements

In order to run this project you will need: 
{{ range $requirement := (ds "config").requirements }}
{{ printf "- [%s][%s] - %s" $requirement.name $requirement.url $requirement.description }}{{ end }}

{{ end}}

{{ if has (ds "config") "usage" }}
## Usage

{{(ds "config").usage }}
{{ end }}

{{ if has (ds "config") "quickstarts" }}
## Quick start

Here's how to get started: 
{{ range $quickstart := (ds "config").quickstarts }}
{{ printf "- `%s` - %s" $quickstart.command $quickstart.description }}{{ end }}

{{ end}}

{{ if has (ds "config") "examples" }}
## Examples

Here are some real world examples: 
{{ range $example := (ds "config").examples }}
{{ printf "- [%s](%s) - %s" $example.name $example.url $example.description }}{{ end }}

{{ end}}

{{ if has (ds "config") "include" }}
{{ range $file := (datasource "config").include -}}
{{ (include "includes" $file) }}
{{- end }}
{{ printf "\n\n" }}
{{- end }}

{{ if has (ds "config") "related_projects" }}
## Related projects

Check out these related projects: 
{{ range $related_project := (ds "config").related_projects }}
{{ printf "- [%s](%s) - %s" $related_project.name $related_project.url $related_project.description }}{{ end }}

{{ end}}

{{ if has (ds "config") "references" }}
## References

For additional context, refer to some of these links. 
{{ range $reference := (ds "config").references }}
{{ printf "- [%s](%s) - %s" $reference.name $reference.url $reference.description }}{{ end }}

{{ end}}

{{ if has (ds "config") "resources" }}
## Resources

Resources used to create this project: 
{{ range $resource := (ds "config").resources }}
{{ printf "- [%s](%s) - %s" $resource.name $resource.url $resource.description }}{{ end }}

{{ end}}


{{ if has (ds "config") "repo" }}
## Repository

We use [SemVer](http://semver.org/) for versioning. 

- **[Branches][branches]**
- **[Commits][commits]**
- **[Tags][tags]**
- **[Contributors][contributors]**
- **[Graph][graph]**
- **[Charts][charts]**

{{ end }}

{{ if has (ds "config") "contributing" }}
{{ if eq (ds "config").contributing "true" }}
{{ include "contributing" }}
{{ end }}
{{ end }}


{{ if has (ds "config") "commercial" }}
{{ if eq (ds "config").commercial "true" }}
{{ include "commercial" }}
{{ end }}
{{ end }}

{{ if has (ds "config") "contributors" }}
## Contributors

Thank you so much for making this project possible: 
{{ range $contributor := (ds "config").contributors }}
{{ printf "- [%s](https://gitlab.com/%s)" $contributor.name $contributor.username }}{{ end }}

{{ end}}

## Copyright

Copyright © 2019-{{ time.Now.Year }} [{{ .Env.COMPANY_NAME }}][company]


{{ if has (ds "config") "license" }}
{{ if eq (ds "config").license "APACHE2" }}
## License 

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) 

See [LICENSE](LICENSE) for full details.

    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.
{{ end }}
{{ if eq (ds "config").license "CC-BY-NC-SA-4.0" }}
## License 

[![License](https://img.shields.io/badge/License-CC%20BY%20NC%20SA%204.0-blue.svg)](https://creativecommons.org/licenses/by-nc-sa/4.0/) 

<a href="https://creativecommons.org/licenses/by-nc-sa/4.0/"><img title="Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License" src="https://docs.cloudposse.com/images/cc-by-nc-sa.png" width="150" /></a>

This material may only be distributed subject to the terms and conditions set forth in the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License or later with the restrictions noted below (the latest version of the license is presently available at <https://creativecommons.org/licenses/by-nc-sa/4.0/>).

**Attribution** You must attribute the work in the manner specified by the author or licensor.

**Noncommercial** The licensor permits others to copy, distribute and transmit the work. In return, licensees may not use the work for commercial purposes — unless they get the licensor's permission.

**Share Alike** The licensor permits others to distribute derivative works only under the same license or one compatible with the one that governs the licensor's work.

## Distribution

Distribution of substantively modified versions of this document is prohibited without the explicit permission of the copyright holder.

Distribution of the work or derivative of the work in any standard (paper) book form for commercial purposes is prohibited unless prior permission is obtained from the copyright holder.
{{ end }}

{{ if eq (ds "config").license "MIT" }}
## License 

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

The MIT License (MIT)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

Source: <https://opensource.org/licenses/MIT>
{{ end }}

{{ if eq (ds "config").license "ISC" }}
## License 

[![License: ISC](https://img.shields.io/badge/License-ISC-blue.svg)](https://opensource.org/licenses/ISC)

ISC License (ISC)

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted, provided that the above copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Source: <https://opensource.org/licenses/ISC>
{{ end }}

{{ if eq (ds "config").license "GPL3" }}
## License 

[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

GNU GENERAL PUBLIC LICENSE  
Version 3, 29 June 2007

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
{{ end }}
{{ end }}

{{ if has (ds "config") "logo" }}
[logo]: {{(ds "config").logo}}
{{else}}
[logo]: https://source.unsplash.com/1280x720/?nature,bird
{{ end }}

[company]: {{ .Env.COMPANY_WEBSITE }}
[contact]: {{ .Env.COMPANY_WEBSITE }}/contact
[services]: {{ .Env.COMPANY_WEBSITE }}/services
[industries]: {{ .Env.COMPANY_WEBSITE }}/industries
[training]: {{ .Env.COMPANY_WEBSITE }}/training
[insights]: {{ .Env.COMPANY_WEBSITE }}/insights
[about]: {{ .Env.COMPANY_WEBSITE }}/about
[join]: {{ .Env.COMPANY_WEBSITE }}/Join-Our-Team

[roots]: https://gitlab.com/skalesys/roots
[ansible]: https://ansible.com
[terraform]: http://terraform.io
[packer]: https://www.packer.io
[docker]: https://www.docker.com/
[vagrant]: https://www.vagrantup.com/
[kubernetes]: https://kubernetes.io/
[spinnaker]: https://www.spinnaker.io/
[jenkins]: https://jenkins.io/
[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/

{{ if has (ds "config") "links" }}

{{ range $link := (ds "config").links }}
{{ printf "[%s]: %s" $link.name $link.url}}{{ end }}

{{ end}}


{{ if has (ds "config") "repo" }}

[branches]: https://gitlab.com/{{(ds "config").repo }}/branches
[commits]: https://gitlab.com/{{(ds "config").repo }}/commits
[tags]: https://gitlab.com/{{(ds "config").repo }}/tags
[contributors]: https://gitlab.com/{{(ds "config").repo }}/graphs
[graph]: https://gitlab.com/{{(ds "config").repo }}/network
[charts]: https://gitlab.com/{{(ds "config").repo }}/charts
{{ end }}


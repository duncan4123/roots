

![logo][logo]



[![pipeline status](https://gitlab.com/skalesys/roots/badges/master/pipeline.svg)](https://gitlab.com/skalesys/roots/commits/master)



 
# Roots 


 
Roots installs tools and builds your project using Makefile targets and Ansible. 


---


## Screenshots


![demo](docs/demo.gif) |
|:--:|
| *How to initialize `roots`* |









## Requirements

In order to run this project you will need: 

- [Ubuntu 18][ubuntu18] - Ubuntu 18.04.2 LTS (Bionic Beaver)
- [Make][make] - The purpose of the make utility is to determine automatically which pieces of a large program need to be recompiled, and issue the commands to recompile them. you can use make with any programming language whose compiler can be run with a shell command.




## Usage

At the top of you `Makefile` include the following lines:
```bash
SHELL = /bin/sh
export ROOTS_VERSION=master
-include $(shell [ ! -d .roots ] && git clone --branch $(ROOTS_VERSION) https://gitlab.com/skalesys/roots.git .roots; echo .roots/Makefile)
```





## Examples

Here are some real world examples: 

- [github-authorized-keys](https://github.com/cloudposse/github-authorized-keys/) - A Golang project that leverages `docker/%`, `go/%`, `travis/%` targets
- [charts](https://github.com/cloudposse/charts/) - A collection of Helm Charts that leverages `docker/%` and `helm/%` targets
- [bastion](https://github.com/cloudposse/bastion/) - A docker image that leverages `docker/%` and `bash/lint` targets
- [terraform-null-label](https://github.com/cloudposse/terraform-null-label/) - A terraform module that leverages `terraform/%` targets




## Makefile targets

```Available targets:

  ansible/requirements               	Install Roles from file requirements.yml
  aws-nuke/install                   	Install aws-nuke
  base                               	Runs base playbook
  clean                              	Clean roots
  code/install                       	Install Visual Studio Code
  docker/build                       	Build docker image
  docker/clean                       	Cleanup docker.                     WARNING!!! IT WILL DELETE ALL UNUSED RESOURCES
  docker/clean/containers            	Cleanup docker containers.          WARNING!!! IT WILL DELETE ALL UNUSED CONTAINERS
  docker/clean/images/all            	Cleanup docker images all.          WARNING!!! IT WILL DELETE ALL IMAGES
  docker/clean/images                	Cleanup docker images.              WARNING!!! IT WILL DELETE ALL UNUSED IMAGES
  docker/clean/networks              	Cleanup docker networks.            WARNING!!! IT WILL DELETE ALL UNUSED NETWORKS
  docker/clean/volumes               	Cleanup docker volumes.             WARNING!!! IT WILL DELETE ALL UNUSED VOLUMES
  docker/image/promote/local         	Promote $SOURCE_DOCKER_REGISTRY/$IMAGE_NAME:$SOURCE_VERSION to $TARGET_DOCKER_REGISTRY/$IMAGE_NAME:$TARGET_VERSION
  docker/image/promote/remote        	Pull $SOURCE_DOCKER_REGISTRY/$IMAGE_NAME:$SOURCE_VERSION and promote to $TARGET_DOCKER_REGISTRY/$IMAGE_NAME:$TARGET_VERSION
  docker/image/push                  	Push $TARGET_DOCKER_REGISTRY/$IMAGE_NAME:$TARGET_VERSION
  docker/install                     	Install docker
  dropbox/install                    	Install Dropbox
  gomplate/install                   	Install Gomplate
  gomplate/version                   	Displays Gomplate version
  google-chrome/install              	Install google-chrome
  help/all                           	Display help for all targets
  help/all/plain                     	Display help for all targets
  help                               	Help screen
  help/short                         	This help short screen
  java/install                       	Install java
  openvpn/install                    	Install openvpn
  packer/install                     	Install packer
  packer/version                     	Prints the packer version
  peek/install                       	Install peek
  peek/version                       	Prints the peek version
  pip/install                        	Install pip
  readme                             	Alias for readme/build
  readme/build                       	Create README.md by building it from README.yaml
  readme/install                     	Install README
  security/install                   	Install security
  spotify/install                    	Install spotify
  terraform/apply                    	Builds or changes infrastructure
  terraform/clean                    	Cleans Terraform vendor from Maker
  terraform/console                  	Interactive console for Terraform interpolations
  terraform/destroy                  	Destroy Terraform-managed infrastructure, removes .terraform and local state files
  terraform/fmt                      	Rewrites config files to canonical format
  terraform/get                      	Download and install modules for the configuration
  terraform/graph                    	Create a visual graph of Terraform resources
  terraform/init                     	Initialize a Terraform working directory
  terraform/init-s3-backend          	Initialize a Terraform working directory with S3 as backend and DynamoDB for locking
  terraform/install                  	Install terraform
  terraform/output                   	Read an output from a state file
  terraform/plan                     	Generate and show an execution plan
  terraform/providers                	Prints a tree of the providers used in the configuration
  terraform/push                     	Upload this Terraform module to Atlas to run
  terraform/refresh                  	Update local state file against real resources
  terraform/show                     	Inspect Terraform state or plan
  terraform/taint                    	Manually mark a resource for recreation
  terraform/untaint                  	Manually unmark a resource as tainted
  terraform/validate                 	Validates the Terraform files
  terraform/version                  	Prints the Terraform version
  terraform/workspace                	Select workspace
  update                             	Updates roots
  vagrant/destroy                    	Stops and deletes all traces of the vagrant machine
  vagrant/install                    	Install vagrant
  vagrant/recreate                   	Destroy and creates the vagrant environment
  vagrant/update/boxes               	Updates all Vagrant boxes
  vagrant/up                         	Starts and provisions the vagrant environment
  version                            	Displays versions of many vendors installed
  virtualbox/install                 	Install virtualbox
  vlc/install                        	Install vlc
```






## Related projects

Check out these related projects: 

- [Packages](https://github.com/cloudposse/packages) - Cloud Posse installer and distribution of native apps
- [Dev Harness](https://github.com/cloudposse/dev) - Cloud Posse Local Development Harness




## References

For additional context, refer to some of these links. 

- [Wikipedia - Test Harness](https://en.wikipedia.org/wiki/Test_harness) - The `build-harness` is similar in concept to a "Test Harness"




## Resources

Resources used to create this project: 

- [Photo](https://unsplash.com/photos/ORYtC6R8omE) - by Dean Truderung on Unsplash
- [Gitignore.io](https://gitignore.io) - Defining the `.gitignore`
- [LunaPic](https://www341.lunapic.com/editor/) - Image editor (used to create the avatar)
- [Contributing](https://gist.github.com/PurpleBooth/b24679402957c63ec426) - CONTRIBUTING.md template
- [EmbedYoutube](http://embedyoutube.org/) - convert Youtube url to GitHub comment





## Repository

We use [SemVer](http://semver.org/) for versioning. 

- **[Branches][branches]**
- **[Commits][commits]**
- **[Tags][tags]**
- **[Contributors][contributors]**
- **[Graph][graph]**
- **[Charts][charts]**









## Contributors

Thank you so much for making this project possible: 

- [Valter Silva](https://gitlab.com/valter-silva)



## Copyright

Copyright © 2019-2019 [SkaleSys][company]




## License 

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) 

See [LICENSE](LICENSE) for full details.

    Licensed to the Apache Software Foundation (ASF) under one
    or more contributor license agreements.  See the NOTICE file
    distributed with this work for additional information
    regarding copyright ownership.  The ASF licenses this file
    to you under the Apache License, Version 2.0 (the
    "License"); you may not use this file except in compliance
    with the License.  You may obtain a copy of the License at

      https://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing,
    software distributed under the License is distributed on an
    "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, either express or implied.  See the License for the
    specific language governing permissions and limitations
    under the License.











[logo]: docs/logo.jpeg


[company]: https://skalesys.com
[contact]: https://skalesys.com/contact
[services]: https://skalesys.com/services
[industries]: https://skalesys.com/industries
[training]: https://skalesys.com/training
[insights]: https://skalesys.com/insights
[about]: https://skalesys.com/about
[join]: https://skalesys.com/Join-Our-Team

[ansible]: https://ansible.com
[terraform]: http://terraform.io
[packer]: https://www.packer.io
[docker]: https://www.docker.com/
[vagrant]: https://www.vagrantup.com/
[kubernetes]: https://kubernetes.io/
[spinnaker]: https://www.spinnaker.io/
[jenkins]: https://jenkins.io/
[aws]: https://aws.amazon.com/
[ubuntu]: https://ubuntu.com/




[ubuntu18]: http://releases.ubuntu.com/18.04/
[make]: https://en.wikipedia.org/wiki/Make_(software)






[branches]: https://gitlab.com/skalesys/roots/branches
[commits]: https://gitlab.com/skalesys/roots/commits
[tags]: https://gitlab.com/skalesys/roots/tags
[contributors]: https://gitlab.com/skalesys/roots/graphs
[graph]: https://gitlab.com/skalesys/roots/network
[charts]: https://gitlab.com/skalesys/roots/charts


